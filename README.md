# Test-Meli-Lambda-Stats

Lambda para obtener estadistica en reto mutantes de mercado libre 

## Manage repository 🤳

- Install dependencies
```
$ npm i
```
- Check code by linter
```
$ npm run lint
```
- Check and fix code by linter
```
$ npm run lint:fix
```
- Check style code
```
$ npm run prettier
```
- Check and fix style code
```
$ npm run prettier:fix
```
- Get coverage test terminal
```
$ npm run coverage
```

## Manage production environment 🎮

- Update Code Lambda
 
```sh
$ aws lambda update-function-code \
 --function-name meli-pro-get-stats-mutants \
 --zip-file fileb://dist/main.zip \
 --publish \
 --output json
```

- Update lambda options
```sh
aws lambda update-function-configuration \
 --function-name meli-pro-get-stats-mutants \
 --environment "Variables={NODE_ENV=pro}" 
```

- Test cors api gateway
 
```sh
curl -v -X OPTIONS https://4gxvefyr7g.execute-api.us-east-2.amazonaws.com/pro/is-mutant
```
- Update Cognito Authenticator ARN

```sh
$ sed -i "s/COGNITO_ID/t232xRr6X/g" apidocs.json
```

- Update Api Gateway Code
 
```sh
$ aws apigateway put-rest-api \
--rest-api-id 4gxvefyr7g \
--cli-binary-format raw-in-base64-out \
--mode merge \
--no-fail-on-warnings \
--body fileb://apidocs.json
```
 
- Update deployment Api Gateway
 
```sh
$ aws apigateway create-deployment \
--rest-api-id 4gxvefyr7g \
--stage-name pro \
--stage-description 'Deployment api 3' 
```
