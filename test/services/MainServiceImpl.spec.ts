import 'reflect-metadata';
import { MainService } from '../../src/services/MainService';
import { MainServiceImpl } from '../../src/services/MainServiceImpl';
import { WinstonLogger } from '../../src/utils/logger/winston/WinstonLogger';
import { ApiGWPresenter } from '../../src/presenters/apigw/ApiGWPresenter';
import { ResponseDatabaseModel } from '../../src/models/ResponseDatabaseModel';

const RESULT_VALID: ResponseDatabaseModel = {
    count_human_dna: 20,
    count_mutant_dna: 20,
};

const RESULT_ZERO: ResponseDatabaseModel = {
    count_human_dna: 0,
    count_mutant_dna: 0,
};

describe('MainService Test Suite', () => {
    const DatabaseAdapterSpy = jasmine.createSpyObj('DatabaseAdapter', ['get']);
    const getDatabaseMock = DatabaseAdapterSpy.get as jasmine.Spy;

    let service: MainService;

    beforeEach(() => {
        service = new MainServiceImpl(
            DatabaseAdapterSpy,
            new ApiGWPresenter(),
            new WinstonLogger()
        );
    });

    it('processData should be return with response OK', async () => {
        getDatabaseMock.and.resolveTo(RESULT_VALID);
        await expectAsync(service.processData()).toBeResolvedTo(
            jasmine.objectContaining({ statusCode: 200 })
        );
    });

    it('processData should be return with response OK with zeros', async () => {
        getDatabaseMock.and.resolveTo(RESULT_ZERO);
        await expectAsync(service.processData()).toBeResolvedTo(
            jasmine.objectContaining({ statusCode: 200 })
        );
    });

    it('processData should be reject with INTERNAL ERROR cause by save Dynamo', async () => {
        getDatabaseMock.and.rejectWith(new Error('INTERNAL ERROR'));
        await expectAsync(service.processData()).toBeResolvedTo(
            jasmine.objectContaining({ statusCode: 500 })
        );
    });
});
