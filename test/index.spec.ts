/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-explicit-any */
import 'reflect-metadata';
import { AppContainer } from '../src/config/Config';
import rewiremock from 'rewiremock';
import { MainController } from '../src/controllers/MainController';
import { TYPES } from '../src/utils/Constants';
import { injectable } from 'inversify';

let calls = 0;
@injectable()
class ControllerMockFail implements MainController {
    constructor() {
        if (calls > 0) {
            throw 'ERROR';
        }
    }
    async handleEvent(): Promise<any> {
        throw new Error('Error Mock');
    }
}

@injectable()
class ControllerMockOK implements MainController {
    constructor() {
        if (calls > 0) {
            throw 'ERROR';
        }
    }
    async handleEvent(): Promise<any> {
        return 'OK';
    }
}

describe('index Test Suite', () => {
    beforeEach(() => {
        calls = 0;
    });
    describe('Success Tests', () => {
        it('handler should return after call the controller', async () => {
            rewiremock.enable();
            const index = require('../src/index');
            rewiremock.disable();
            AppContainer.unbind(TYPES.MainController);
            AppContainer.bind<MainController>(TYPES.MainController).to(ControllerMockOK);
            await expectAsync(index.handler({}, {})).toBeResolved();
        });

        it('handler subsequent calls should not create new instances', async () => {
            rewiremock.enable();
            const index = require('../src/index');
            rewiremock.disable();
            AppContainer.unbind(TYPES.MainController);
            AppContainer.bind<MainController>(TYPES.MainController).to(ControllerMockOK);
            while (calls < 2) {
                await expectAsync(index.handler({}, {})).toBeResolved();
                calls++;
            }
        });

        it('handler should return if unbind fails', async () => {
            rewiremock.enable();
            const index = require('../src/index');
            rewiremock.disable();
            AppContainer.unbind(TYPES.MainController);
            AppContainer.bind<MainController>(TYPES.MainController).to(ControllerMockOK);
            spyOn(AppContainer, 'unbind').and.throwError('Error');
            await expectAsync(index.handler({}, {})).toBeResolved();
        });
    });
    describe('Error Tests', () => {
        it('handler should throw if an error happens', async () => {
            rewiremock.enable();
            const index = require('../src/index');
            rewiremock.disable();
            AppContainer.unbind(TYPES.MainController);
            AppContainer.bind<MainController>(TYPES.MainController).to(ControllerMockFail);
            await expectAsync(index.handler({}, {})).toBeRejectedWithError();
        });
    });
});
