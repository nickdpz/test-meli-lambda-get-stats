/* eslint-disable @typescript-eslint/no-explicit-any */
export interface MainController {
    handleEvent(): Promise<any>;
}
