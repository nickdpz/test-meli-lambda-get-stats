import { MainController } from '../MainController';
import { inject, injectable } from 'inversify';
import { MainService } from '../../services/MainService';
import { TYPES } from '../../utils/Constants';

@injectable()
export class ApiGWController implements MainController {
    constructor(
        @inject(TYPES.MainService)
        private service: MainService
    ) {}

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async handleEvent(): Promise<any> {
        return this.service.processData();
    }
}
