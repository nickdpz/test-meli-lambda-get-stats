/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import 'reflect-metadata';
import { AppContainer } from './config/Config';
import { TYPES } from './utils/Constants';
import { MainController } from './controllers/MainController';
import { WinstonLogger } from './utils/logger/winston/WinstonLogger';

let controllerInstance: MainController;

export const handler = async (event: any, context: any): Promise<any> => {
    const logger = new WinstonLogger();
    logger.debug('Evento Recibido: %o \n Contexto de Ejecución: %o', event, context);
    try {
        controllerInstance =
            controllerInstance ?? AppContainer.get<MainController>(TYPES.MainController);
        return await controllerInstance.handleEvent();
    } catch (error) {
        throw error;
    }
};
