export const HTTP_CODES = {
    OK: 200,
    ERROR_REQUEST: 400,
    ERROR_INTERNAL: 500,
};

export const CONSTANTS = {
    LOG_LEVEL: 'debug',
    TIMEZONE: 'America/Bogota',
    TABLE_NAME: `meli-${process.env.NODE_ENV}-mutants-stats-table`,
    DATA_TYPE: 'STATS',
    DATE: '2021-12-19T20:10:56.739Z',
};

export const TYPES = {
    MainPresenter: Symbol.for('MainPresenter'),
    MainService: Symbol.for('MainService'),
    MainController: Symbol.for('MainController'),
    Logger: Symbol.for('Logger'),
    DatabaseAdapter: Symbol.for('DatabaseAdapter'),
};
