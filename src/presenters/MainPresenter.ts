/* eslint-disable @typescript-eslint/no-explicit-any */
import { ResponseApiGWModel } from '../models/ResponseApiGWModel';
export interface MainPresenter {
    generateSuccessResponse(body?: any): ResponseApiGWModel;
    generateInternalErrorResponse(message: string): ResponseApiGWModel;
    generateErrorRequestResponse(message: string): ResponseApiGWModel;
}
