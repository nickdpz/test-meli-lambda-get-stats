module.exports = {
	parser: '@typescript-eslint/parser', // Specifies the ESLint parser
	parserOptions: {
		ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
		sourceType: 'module', // Allows for the use of imports
		//warnOnUnsupportedTypeScriptVersion: false
	},
	env: {
		jasmine: true,
	},
	plugins: ['jasmine'],
	extends: [
		'plugin:@typescript-eslint/recommended', // Uses the recommended rules from the @typescript-eslint/eslint-plugin
	],
	rules: {
		'@typescript-eslint/camelcase': 'off', //axios-imp.consumer.ts access_token
	},
};
